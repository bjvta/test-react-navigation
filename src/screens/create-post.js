// src/screens/create-post.js
import React from 'react';
import {TextInput, StyleSheet, Button} from 'react-native';

function CreatePostScreen({route, navigation}) {
  const [postText, setPostText] = React.useState('');
  const goBackToHome = () => navigation.navigate('Home', {post: postText});
  console.log(route);
  return (
    <>
      <TextInput
        multiline
        placeholder="Type here"
        style={styles.textInput}
        value={postText}
        onChangeText={setPostText}
      />
      <Button
        title="Done"
        onPress={goBackToHome}
      />
    </>
  );
}

const styles = StyleSheet.create({
  textInput: {
    height: 200,
    padding: 10,
    backgroundColor: 'white',
  },
});

export default CreatePostScreen;
