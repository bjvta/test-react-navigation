// src/screens/ad-detail.js
import React, {Component} from 'react';
import {View, Text} from 'react-native';
import HomeButton from '../components/home-button';

class AdDetailScreen extends Component {
  render() {
    const {itemId} = this.props.route.params;
    const {title} = this.props.route.params;
    const {category} = this.props.route.params;
    return (
      <View>
        <Text>Item id: {itemId}</Text>
        <Text>Title: {title}</Text>
        <Text>Category: {category}</Text>
        <HomeButton {...this.props} />
      </View>
    );
  }
}

export default AdDetailScreen;
