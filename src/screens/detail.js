// src/screens/detail.js
import React from 'react';
import {Button, Text, View, StyleSheet} from 'react-native';

function DetailScreen({route, navigation}) {
  const {itemId} = route.params;
  const {otherParam} = route.params;

  const goToDetailAgain = () =>
    navigation.push('Detail', {
      itemId: itemId,
      otherParam: otherParam,
    });
  const goBack = () => navigation.goBack();
  const goToHome = () => navigation.navigate('Home');
  const goToTopStack = () => navigation.popToTop();
  return (
    <View style={styles.container}>
      <Text>Detail View</Text>
      <Text>{itemId}</Text>
      <Text>{otherParam}</Text>
      <Button title="Go to the Details again" onPress={goToDetailAgain} />
      <Button title="Go Back" onPress={goBack} />
      <Button title="Go to Home" onPress={goToHome} />
      <Button title="Go back to first screen in stack" onPress={goToTopStack} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default DetailScreen;
