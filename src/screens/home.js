// src/screens/home.js
import React from 'react';
import {Button, Text, View, SafeAreaView, TextInput, ScrollView} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

// local components
import HomeButton from '../components/home-button';
import PublicityList from '../components/publicity-list';
import FeaturedList from '../components/featured-list';
import LatestList from '../components/latest-list';

function HomeScreen({route, navigation}) {
  // const updateTitle = () => navigation.setOptions({title: 'Updated!'})
  // // const openNav = () => navigation.openDrawer();
  // const openModal = () => navigation.navigate('Modal');
  // const goTo = () =>
  //   navigation.navigate('Detail', {
  //     itemId: 86,
  //     otherParam: 'whatever you want',
  //   });
  // const goToCreatePostScreen = () => navigation.navigate('CreatePost');
  const goToAdDetail = () =>
    navigation.navigate('AdDetail', {
      itemId: 20,
      title: 'Nombre del Producto',
      category: 'This is the category',
    });

  React.useEffect(() => {
    if (route.params?.post) {
      // Post updated,
      // Example to use, send the post to the server
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [route.params?.post]);

  return (
    <SafeAreaView>
      <ScrollView>
        <PublicityList />
        <View style={{marginLeft: 5, marginRight: 5, marginBottom: 5}}>
          <TextInput
            style={{
              height: hp('5%'),
              borderColor: '#00B200',
              borderWidth: 1,
              borderRadius: 5,
              fontSize: hp('2%'),
            }}
            placeholder="¿Qué estás buscando?"
          />
        </View>
        <FeaturedList {...{navigation, route}} />
        {/*<Button title="Open Menu" onPress={openNav}></Button>*/}
        {/*<Button title="Go to Details" onPress={goTo} />*/}
        {/*<Button title="Create a Post" onPress={goToCreatePostScreen} />*/}
        {/*<Button title="Update the title" onPress={updateTitle} />*/}
        {/*<Button title="Open the modal" onPress={openModal} />*/}
        <LatestList />
      </ScrollView>
      <HomeButton {...{navigation, route}} />
    </SafeAreaView>
  );
}

export default HomeScreen;
