import React, {Component} from 'react';
import {View, FlatList, Text, Image, Dimensions} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

function PublicityItem() {
  return (
    <View>
      <Image
        style={{
          width: wp('100%'),
          resizeMode: 'contain',
          height: hp('22%'),
        }}
        source={require('../assets/images/publicity-image.png')}
      />
    </View>
  );
}

const dataList = [
  {
    id: '1',
    title: 'Title 1',
  },
  {
    id: '2',
    title: 'Title 1',
  },
  {
    id: '3',
    title: 'Title 1',
  },
];

class PublicityList extends Component {
  render() {
    return (
      <View>
        <FlatList
          horizontal
          data={dataList}
          renderItem={({item}) => <PublicityItem />}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

export default PublicityList;
