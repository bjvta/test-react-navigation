import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Platform,
  TouchableOpacity,
} from 'react-native';

function HomeButton(props) {
  return (
    <View style={styles.homeButton}>
      <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
        <View>
          <Text style={{color: 'white'}}>Inicio</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const customMarginBottom = Platform.OS === 'ios' ? 200 : 170;

const styles = StyleSheet.create({
  container: {},
  homeButton: {
    top: Dimensions.get('window').height - customMarginBottom,
    left: Dimensions.get('window').width / 2 - 30,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
    backgroundColor: '#00B200',
    borderRadius: 50,
  },
});

export default HomeButton;
