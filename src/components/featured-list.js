import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';

class FeaturedItem extends Component {
  render() {
    const onPress = () =>
      this.props.navigation.navigate('AdDetail', {
        itemId: 20,
        title: 'Nombre del Producto',
        category: 'This is the category',
      });

    return (
      <TouchableOpacity onPress={onPress}>
        <View>
          <Image
            style={{
              margin: 1,
              resizeMode: 'contain',
            }}
            source={require('../assets/images/tractor.png')}
          />
          <View
            style={{
              position: 'absolute',
              flex: 1,
              top: 110,
              backgroundColor: 'rgba(65, 189, 0, 0.5)',
              width: 195,
            }}>
            <Text style={{color: 'white', fontSize: 16, fontWeight: '500'}}>
              Nombre del Producto
            </Text>
            <Text style={{fontSize: 12}}>Categoría</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const data = [
  {
    id: '1',
    title: 'Title 1',
  },
  {
    id: '2',
    title: 'Title 1',
  },
  {
    id: '3',
    title: 'Title 1',
  },
];

class FeaturedList extends Component {
  render() {
    return (
      <SafeAreaView>
        <View>
          <Text style={{fontSize: 20, color: '#00B200'}}>DESTACADOS</Text>
          <FlatList
            horizontal
            data={data}
            renderItem={({item}) => <FeaturedItem {...this.props} />}
            keyExtractor={item => item.id}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default FeaturedList;
