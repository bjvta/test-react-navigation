import React from 'react';
import {Image, View, Button, StyleSheet, SafeAreaView} from 'react-native';

function Header(props) {
  const isNavigationUndefined = props.navigation === undefined;
  const backButton = (
    <View style={styles.backButton}>
      <Button
        title=""
        color="white"
        onPress={() => props.navigation.navigate(props.previous.route.name)}
      />
    </View>
  );
  const noBackButton = <></>;
  return (
    <View>
      <SafeAreaView>
        <View style={styles.container}>
          {isNavigationUndefined ? noBackButton : backButton}
          <Image
            source={require('../assets/images/logo-1.png')}
            style={styles.logo}
          />
          <View style={styles.right}>
            {props.children}
            <Image
              source={require('../assets/images/menu-icon.png')}
              style={styles.menuIcon}
            />
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  logo: {
    width: 220,
    height: 60,
    marginLeft: -10,
    resizeMode: 'contain',
  },
  menuIcon: {
    marginTop: 10,
    marginRight: 10,
    resizeMode: 'contain',
  },
  container: {
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    backgroundColor: '#00B200',
    flexDirection: 'row',
  },
  right: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  backButton: {
    display: 'none',
    width: 30,
    fontSize: 20,
    marginTop: 10,
  },
});

export default Header;
