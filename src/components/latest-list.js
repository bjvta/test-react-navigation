import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

class LatestItem extends Component {
  render() {
    return (
      <TouchableOpacity>
        <View>
          <Image
            style={{
              margin: 1,
              resizeMode: 'contain',
              width: 205,
            }}
            source={require('../assets/images/tractor.png')}
          />
          <View
            style={{
              position: 'absolute',
              top: 110,
              left: 5,
              backgroundColor: 'rgba(65, 189, 0, 0.5)',
              width: 195,
            }}>
            <Text style={{color: 'white', fontSize: 16, fontWeight: '500'}}>
              Nombre del Producto
            </Text>
            <Text style={{fontSize: 12}}>Categoría</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const data = [
  {
    id: '1',
    title: 'Title 1',
  },
  {
    id: '2',
    title: 'Title 1',
  },
  {
    id: '3',
    title: 'Title 1',
  },
  {
    id: '4',
    title: 'Title 1',
  },
  {
    id: '5',
    title: 'Title 1',
  },
  {
    id: '6',
    title: 'Title 1',
  },
  {
    id: '7',
    title: 'Title 1',
  },
  {
    id: '8',
    title: 'Title 1',
  },
  {
    id: '9',
    title: 'Title 1',
  },
  {
    id: '10',
    title: 'Title 1',
  },
  {
    id: '11',
    title: 'Title 1',
  },
  {
    id: '12',
    title: 'Title 1',
  },
  {
    id: '13',
    title: 'Title 1',
  },
  {
    id: '14',
    title: 'Title 1',
  },
  {
    id: '15',
    title: 'Title 1',
  },
  {
    id: '16',
    title: 'Title 1',
  },
  {
    id: '17',
    title: 'Title 1',
  },
  {
    id: '18',
    title: 'Title 1',
  },
  {
    id: '19',
    title: 'Title 1',
  },
  {
    id: '20',
    title: 'Title 1',
  },
];

class LatestList extends Component {
  render() {
    return (
      <View>
        <Text style={{fontSize: 20, color: '#00B200', marginLeft: 5}}>
          ÚLTIMOS APORTES
        </Text>
        <FlatList
          numColumns={2}
          data={data}
          renderItem={({item}) => <LatestItem />}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

export default LatestList;
