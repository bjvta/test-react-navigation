# Agrotratos App

## Requirements

- Node
- Yarn
- Android Studio
- XCode (Mac OS)

## Run the project

After cloning the project, go into the folder and run:
```
npm install
```
If you do not want to use `npm` you can use `yarn`
```
yarn install
```

### Launch the project

For android
```
react-native run-android
```

For iOS
```
react-native run-ios
```
