// App.js
import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Text} from 'react-native';

// src
import HomeScreen from './src/screens/home';
import DetailScreen from './src/screens/detail';
import CreatePostScreen from './src/screens/create-post';
import ModalScreen from './src/screens/modal';
import Header from './src/components/header';
import AdDetailScreen from './src/screens/ad-detail';

const RootStack = createStackNavigator();
const MainStack = createStackNavigator();

function MainStackScreen() {
  return (
    <MainStack.Navigator>
      <MainStack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          header: props =>
            <Header>
              <Text
                style={{
                  color: 'white',
                  padding: 20,
                  fontSize: 18,
                  fontWeight: 'bold',
                }}>
                Inicio
              </Text>
            </Header>
        }}
      />
      {/*<MainStack.Screen*/}
      {/*  name="Detail"*/}
      {/*  component={DetailScreen}*/}
      {/*  options={{header: props => <Header {...props} /> }}*/}
      {/*/>*/}
      <MainStack.Screen
        name="AdDetail"
        component={AdDetailScreen}
        options={{header: props => <Header {...props} /> }}
      />
      <MainStack.Screen name="CreatePost" component={CreatePostScreen} />
    </MainStack.Navigator>
  );
}

function RootStackScreen() {
  return (
    <RootStack.Navigator mode="modal" headerMode="none">
      <RootStack.Screen
        name="Main"
        component={MainStackScreen}
        options={{headerShown: false}}
      />
      <RootStack.Screen name="Modal" component={ModalScreen} />
    </RootStack.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );
}

export default App;
